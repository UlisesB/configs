#!/usr/bin/bash

[ ${UID} -eq 0 ] && printf "Don't run this script as root!\n" && exit 1

[ "${1}" = "-b" ] && make_backup="yes"

dir=$(realpath $(dirname ${0}))

log=${dir}/configs.log
printf '' > ${log}

conf_dir=${XDG_CONFIG_HOME:-${HOME}/.config}

excepts_file=${dir}/configs.excepts
[ -f ${excepts_file} ] || printf '' > ${excepts_file}

source ${excepts_file}

cd ${dir}

# Prints $1 and appends it to $log
conf_log() {
    printf "${1}\n" | tee -a ${log}
}

# Sets $dot_dir to the dir $1 should be copied to
# ($conf_dir if $1 is not in $excepts_file)
conf_getdir() {
    for exception in ${exceptions}; do
        exc_file=$(printf "${exception}\n" | cut -d "${exc_delim}" -f 1)
         exc_dir=$(printf "${exception}\n" | cut -d "${exc_delim}" -f 2)

        if [ ${exc_file} = ${1} ]; then
            conf_log "info: ${1} is in ${excepts_file}"
            dot_dir=${exc_dir}
            return 0
        fi
    done
    dot_dir=${conf_dir}/$(dirname ${1})
}

# Makes sure (dir) $1 exists
conf_checkdir() {
    if [ -d ${1} ]; then
        conf_log "info: ${1}/ already exists"
    else
        conf_log "info: ${1}/ doesn't already exist"
        mkdir ${1} && \
        conf_log "log: created ${1}/"
    fi
}

# Backs up $1
conf_backup() {
    if [ -f ${1} ]; then
        conf_log "info: ${1} already exists"
        mv ${1} ${1}.bk && \
        conf_log "log: backed up ${1}"
    fi
}

# Copies $1 to $2
conf_copy() {
        cp -np --reflink=auto ${1} ${2}
        conf_log "log: copied ${1} to ${2}"
}

conf_main() {
    for dot_file in $(find ./*/ -type f | cut -c 3-); do
        conf_getdir ${dot_file}
        conf_checkdir ${dot_dir}
        [ "${make_backup}" = "yes" ] && \
        conf_backup ${dot_dir}/$(basename ${dot_file})
        conf_copy ${dot_file} ${dot_dir}
    done
    conf_log "info: finished"
}

conf_main
exit 0
