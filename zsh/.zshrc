source ${ZDOTDIR}/.zalias

bindkey -v
zstyle :compinstall filename '/home/ulises/.config/zsh/.zshrc'

autoload -Uz compinit
compinit

unsetopt NOTIFY
# NOTIFY: Report status of bg jobs immediately instead of only before a prompt

setopt AUTO_CD CD_SILENT CHASE_LINKS HIST_IGNORE_ALL_DUPS HIST_NO_STORE \
INTERACTIVE_COMMENTS BSD_ECHO BEEP
# HIST_IGNORE_ALL_DUPS: Delete duplicates no matter how old they are
# HIST_NO_STORE: Remove the history command from the history
# INTERACTIVE_COMMENTS: So comments work on interactive shells
# BSD_ECHO: So ESC sequences only work with the -e flag
