# Configs

My configs + deployment script

By default dotfiles are copied to $XDG_CONFIG_HOME
(or $HOME/.config if it doesn't exist).
This can be overriden per file by editing the
configs.excepts file (instructions in the file)
