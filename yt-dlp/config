##### GENERAL OPTIONS #####

# Continue with the next video if an error occurs
--no-abort-on-error

##### GEO-RESTRICTION #####

# Bypass geographic restriction
--geo-bypass

##### VIDEO SELECTION #####

# Only download videos from the past week
--dateafter "now-1week"

# Filters
--no-match-filter

# Write downloaded videos to a file so they don't get downloaded again
--download-archive "${HOME}/Videos/YouTube/archive.txt"

# Stop downloading after a file gets filtered out
--break-on-reject

##### DOWNLOAD OPTIONS #####

# Download rate limit
--limit-rate 6M

# Delete downloaded fragments after downloading
--no-keep-fragments

# Download buffer size
--buffer-size 2048

# Resize the buffer automatically from the initial value
--resize-buffer

# Do not reverse playlist download order
--no-playlist-reverse

##### FILESYSTEM OPTIONS #####

# Output file
--output "${HOME}/Videos/YouTube/%(channel)s %(title)s.%(ext)s"

# Restrict video file name to ASCII characters
--restrict-filenames

# Do not make filenames Windows compatible
--no-windows-filenames

# Do not override video files, only related ones
--no-force-overwrites

# Resume partially downloaded files/fragments
--continue

# Use .part files instead of writing directly to the video file
--part

# Write the description to a .description file
--write-description

# Do not write video metadata
--no-write-info-json

# Do not write playlist metadata
--no-write-playlist-metafiles

# Do not write the comments
--no-write-comments

# Do not read/dump cookies
--no-cookies

# Do not load cookies from browser
--no-cookies-from-browser

##### THUMBNAIL OPTIONS #####

# Write thumbnail
--write-thumbnail

##### VERBOSITY AND SIMULATION OPTIONS #####

# Quiet mode
#--quiet

##### VIDEO FORMAT OPTIONS #####

# Format code
--format 'bv*[height<=1080][fps<=60]+ba/b[height<=1080][fps<=60] / wv*+wa/w'

# Prefer video formats with free containers and same quality
--prefer-free-formats

##### SUBTITLE OPTIONS #####

# Write subtitles
--write-subs

# Do not write automatically generated subtitles
--no-write-auto-subs

# Write only english subtitles
--sub-langs en.*

##### POST-PROCESSING OPTIONS #####

# Delete the intermediate video file after the merge
--no-keep-video

# Do not overwrite post-processed files
--no-post-overwrites

# Do not embed subtitles
--no-embed-subs

# Do not embed thumbnail
--no-embed-thumbnail

# Do not embed metadata
--no-embed-metadata

# Do not embed chapter markers
--no-embed-chapters

# Fault correction policy
--fixup detect_or_warn

# Convert the thumbnails to PNG
--convert-thumbnails png

# Do not split video into multiple files based on chapters
--no-split-chapters

# Do not remove any chapters
--no-remove-chapters

##### SPONSORBLOCK OPTIONS #####

# Remove sponsors and self promotions
#--sponsorblock-remove sponsor,selfpromo
